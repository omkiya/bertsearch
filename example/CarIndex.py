
"""
Car Indexer
===========

Car indexing example for testing purposes.

:Authors: Krisztian Balog, Faegheh Hasibi
small edits by Omid Mohammadi Kia
"""
#from elastic import Elastic
import json
#from elasticsearch import Elasticsearch
#from config import ELASTIC_HOSTS, ELASTIC_SETTINGS
import csv
from csv import DictWriter


def append_dict_as_row(file_name, dict_of_elem, field_names):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        dict_writer = DictWriter(write_obj, fieldnames=field_names)
        # Add dictionary as wor in the csv
        dict_writer.writerow(dict_of_elem)


def main():

    body = 0
    field_names = ['Id', 'text']
    #first we should convert cbor file to xml
    #using trec car tools
    #https://github.com/gla-ial/trec-cast-tools
    with open("/media/ntfs/cbor.xml") as infile:
        i=0
        dic = {}
        for line in infile:
            if (line.startswith("<DOCNO>")):
                left = "<DOCNO>"
                right = "</DOCNO>"
                docid = line[line.index(left) + len(left):line.index(right)]

            elif (line.startswith("<BODY>")):
                body = 1
            #before and after body is new line we extract only body
            elif (body >= 1):
                body = body + 1
                if (body == 2):
                    bod = line.rstrip()
                    body = 0

                    dic['text'] = bod.replace(",", " ")
                    dic['Id'] = docid
                    append_dict_as_row('treccar.csv', dic, field_names)
                    dic.clear()

            else:
                body = 0

if __name__ == "__main__":
    main()





