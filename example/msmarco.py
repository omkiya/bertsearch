"""
msmarco Indexer
===========

msmarco indexing example for testing purposes.

:Authors: Krisztian Balog, Faegheh Hasibi
small edits by Omid Mohammadi Kia
"""
import json
import csv
from csv import DictWriter


def append_dict_as_row(file_name, dict_of_elem, field_names):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        dict_writer = DictWriter(write_obj, fieldnames=field_names)
        # Add dictionary as wor in the csv
        dict_writer.writerow(dict_of_elem)


def main():
    field_names = ['Id', 'text']

    duparray=[]
    filepath = "/home/saha/Documents/bert/duplicate_list_v1.0.txt"
    with open(filepath) as fp:
        line = fp.readline()
        while line:
            x = line.replace(":", ",").replace("\n",'' ).split(',')
            if(x[1:]!=['']):
                duparray.extend(x[1:])
            line = fp.readline()
    with open("/home/saha/Documents/bert/collection.tsv", encoding='utf-8') as tsvfile:
        tsvreader = csv.reader(tsvfile, delimiter="\t")
        doc = {}
        for line in tsvreader:

            if (not "MARCO_" + line[0] in duparray):
                doc['text'] = line[1].replace(",", " ")
                doc['Id'] = "MARCO_" + line[0]
            append_dict_as_row('msmarco.csv', doc, field_names)
            doc.clear()

if __name__ == "__main__":
    main()





